package com.example.tanvi.buildvarients;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.android.application.Constants;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (Constants.type == Constants.Type.FREE){
            Toast.makeText(MainActivity.this,"Free Version",Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(MainActivity.this,"Paid Version",Toast.LENGTH_SHORT).show();
        }
    }
}
